//Load all the required libraries
var path = require("path");
var bodyParser = require("body-parser");
var express = require("express");

//Define a list of static resources
var staticDirs = [ "public", "demos", "bower_components" ];

//Create an instance of the app
var app = express();

//Install json middleware 
app.use(bodyParser.json());

//Add list of directories to route
for (var i in staticDirs)
	app.use(express.static(path.join(__dirname, staticDirs[i])));

//Define the port to listen to
app.set("port", process.env.APP_PORT || 3000);

//Start the application
app.listen(app.get("port"), function() {
	console.info("Application started at %s on port %d"
			, new Date(), app.get("port"));
});
