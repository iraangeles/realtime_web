(function() {
	var WSChatApp = angular.module("WSChatApp", []);

	var WSChatCtrl = function($scope) {
		var wsChatCtrl = this;

		wsChatCtrl.chats = "";
		wsChatCtrl.message = "";
		wsChatCtrl.name = "";

		wsChatCtrl.send = function() {
		};
	};

	WSChatApp.controller("WSChatCtrl", [ "$scope", WSChatCtrl ]);
})();
