(function() {
	var ChessApp = angular.module("ChessApp", []);

	var ChessSvc = function($rootScope, $http) {
		var chessSvc = this;

		chessSvc.socket = null;

		chessSvc.createGame = function() {
			return ($http.post("/chess")
				.then(function(result) {
					return (result.data.gameId);
				})
			);
		}

		chessSvc.connect = function(gameId, color) {
			chessSvc.socket = new WebSocket("ws://localhost:3000/chess/" + gameId + "/" + color);

			chessSvc.socket.onmessage = function(message) {
				$rootScope.$apply(function() {
					$rootScope.$broadcast("Chess:move", message.data);
				});
			};

			chessSvc.socket.onopen = function() {
				$rootScope.$apply(function() {
					$rootScope.$broadcast("Chess:start", gameId, color);
				});
			}
		}

		chessSvc.joinGame = function(gameId) {
			return ($http.put("/chess", {gameId: gameId}));
		}

		chessSvc.move = function(from, to) {
			chessSvc.socket.send(from + "-" + to);
		}
	};

	var ChessCtrl = function($scope, ChessSvc) {

		var chessCtrl = this;

		chessCtrl.gameId = "no game";

		$scope.$on("Chess:start", function(_, gameId, color) {
			var onDrop = function(from, to) {
				ChessSvc.move(from, to);
			};

			chessCtrl.chessboard = ChessBoard("#board1", {
				position: "start",
				orientation: color,
				pieceTheme: "/chessboardjs/img/chesspieces/wikipedia/{piece}.png",
				draggable: true,
				onDrop: onDrop
			});
		});

		$scope.$on("Chess:move", function(_, chessMove) {
			chessCtrl.chessboard.move(chessMove);
		});

		chessCtrl.createGame = function() {
			ChessSvc.createGame()
				.then(function(gid) {
					chessCtrl.gameId = gid;
					return (gid); })

				.then(function(gid) {
					ChessSvc.connect(gid, "white");
				});
		};

		chessCtrl.joinGame = function() {
			ChessSvc.joinGame(chessCtrl.gameId)
				.then(function() {
					ChessSvc.connect(chessCtrl.gameId, "black");
				})
				.catch(function() {
					chessCtrl.gameId = "Not found";
				});
		};
	};

	ChessApp.service("ChessSvc", [ "$rootScope", "$http", ChessSvc ]);

	ChessApp.controller("ChessCtrl", [ "$scope", "ChessSvc", ChessCtrl ]);
})();
