//Load all the required libraries
var path = require("path");
var uuid = require("node-uuid");
var bodyParser = require("body-parser");
var expressWS = require("express-ws");
var express = require("express");

//Define a list of static resources
var staticDirs = [ "public", "demos", "bower_components" ];

//Create an instance of the app
var app = express();

var wsApp = expressWS(app);

var games = {};

//Install json middleware 
app.use(bodyParser.json());

//Add list of directories to route
for (var i in staticDirs)
	app.use(express.static(path.join(__dirname, staticDirs[i])));

app.post("/chess", function(req, res) {
	var gid = uuid.v4().substring(0, 8);
	games[gid] = [];

	res.status(201)
		.json({gameId: gid});
});

app.put("/chess", function(req, res) {
	var gameId = req.body.gameId;
	if (games[gameId])
		res.status(201).end();
	else
		res.status(404).end();
});

app.ws("/chess/:gameId/:color", function(ws, req) {
	var gameId = req.params.gameId;
	var color = req.params.color;

	ws.gameId = gameId;
	ws.color = color;

	games[gameId].push(ws);

	ws.on("message", function(message) {
		for (var i in games[gameId])
			games[gameId][i].send(message);
	});

	ws.on("close", function() {
		//Perform cleanup
	});

	if (games[gameId])
		games[gameId][color] = ws;
});

//Define the port to listen to
app.set("port", process.env.APP_PORT || 3000);

//Start the application
app.listen(app.get("port"), function() {
	console.info("Chess server started at %s on port %d"
			, new Date(), app.get("port"));
});
